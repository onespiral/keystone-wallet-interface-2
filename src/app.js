import React, { Component } from 'react';
import { withRouter, BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import DefaultPage from './components/DefaultPage';
import ChainOverview from './components/Chain/overview';
import LastBlock from './components/Chain/last-block';
import NBlock from './components/Chain/n-block';
import CreateWallet from './components/Wallet/create';
import Roadmap from './components/Roadmap';
import Litepaper from './components/Litepaper';
import RequireAuth from './components/Auth/RequireAuth';
import SignIn from './components/Auth/SignIn';
import FAQ from './components/FAQ';
import WalletOverview from './components/Wallet/overview';
import SendTransaction from './components/Wallet/send';
import NTransaction from './components/Transactions/n-transaction';
import Investors from './components/Investors';
import AboutUs from './components/AboutUs';
import Exchange from './components/Exchange';
import ProjectTortuga from './components/ProjectTortuga';
import Notfound from './components/not-found';
import NodeList from './components/NodeList';


import 'bootstrap/dist/css/bootstrap.css';
import './index.css';
class App extends Component {
  render() {
    return (
      <Router >
        <Switch>
          <Route path="/" component={DefaultPage} exact />
          <Route path="/Wallet/Create" component={CreateWallet}  exact />
          <Route path="/Roadmap" component={Roadmap} exact />
          <Route path="/Investors" component={Investors} exact  />
          <Route path="/About-Us" component={AboutUs} exact  />
          <Route path="/NodeList" component={NodeList} exact  />
          <Route path="/Litepaper" component={Litepaper} exact  />
          <Route path="/Exchange" component={Exchange} exact  />
          <Route path="/Project-Tortuga" component={ProjectTortuga} exact  />
          <Route path="/Block" component={ChainOverview} exact/>
          <Route path="/Block/Last-Block" component={LastBlock} exact/>
          <Route path="/Block/:blockId" component={NBlock} exact  />
          <Route path="/Transactions/To-Process" component={ChainOverview}  exact />
          <Route path="/Transactions/Last-Transaction" component={ChainOverview}  exact />
          <Route path="/Transactions/LastFifteenTransactions" component={ChainOverview}  exact />
          <Route path="/Transactions/Index/:transactionIndex" component={NTransaction} exact  />
          <Route path="/Wallet" component={RequireAuth(WalletOverview)} exact />
          <Route path="/Wallet/Send-Transaction" component={RequireAuth(SendTransaction)} exact />
          <Route path="/Sign-In" component={SignIn} exact  />
          <Route path="/FAQ" component={FAQ} exact  />
          <Route component={Notfound} />
        </Switch>
      </Router>
    );
  }
}

export default withRouter(App);
