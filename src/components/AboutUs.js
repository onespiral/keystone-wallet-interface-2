import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import Header from './Header';
import Footer from './Footer';
import avatar from '../assets/images/avatar.png';
class AboutUs extends Component {
    render() {
      return (
  <div className="">
   <Header />
    <section>
     <div className="container-fluid" id="aboutus">
      <div className="container">
       <div className="row ">
        <div className="col text-center">
          <h1 className="display-4 color7">About Us</h1>

          <h3  className="color7 text-left display-6">Leadership</h3>
          <p className="color14 text-justify">Keystone’s Leadership collectively brings over 40 years of experience in product development, marketing,  consulting and software engineering</p>
          <h3  className="color7 text-left display-6">Our Team</h3>
          <p className="color14 text-justify">Keystone Currency merges the blockchain with sustainable development; providing new perspective on  quality-of-life in the 21st Century.
          Our team structure is web-based and international. Allowing us to bring in talent across the world for our mission of high-tech sustainability.</p>
      
        <div className="col text-center">
          <p className="color7 display-4">The family of Keystone Currency.</p>
  </div></div>
        </div>
        <div className="row ">
          <div className="col">
            <section className="team">
              <div className="container">
                  <div className="row pt-md">

                    <div className="col-lg-4 col-md-4 col-sm-4 col-12 profile">
                      <div className="text-center"><img className="img-responsive w-50" src={avatar} alt="" /></div>
                      <div className="">
                        <h1 className="color17 display-3"><strong>Lyle (SeeUlookn)</strong></h1>
                        <sup className="color7">Chief Executive Officer</sup>
                      </div>
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-4 col-12 profile">
                      <div className="text-center"><img className="img-responsive w-50" src={avatar} alt="" /></div>
                      <h1 className="color17 display-3"><strong>Scott (Brody)</strong></h1>
                      <sup className="color7">Chief Operations Officer</sup>
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-4 col-12 profile">
                      <div className="text-center"><img className="img-responsive w-50" src={avatar} alt="" /></div>
                      <h1 className="color17 display-3"><strong>Michael (Dreamingrainbow)</strong></h1>
                      <sup className="color7">Chief Technical Officer</sup>
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-4 col-12 profile">
                      <div className="text-center"><img className="img-responsive w-50" src={avatar} alt="" /></div>
                      <h1 className="color17 display-3"><strong>Gavin (Heisenberg)</strong></h1>
                      <sup className="color7">Chief Financial Officer</sup>
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-4 col-12 profile">
                      <div className="text-center"><img className="img-responsive w-50" src={avatar} alt="" /></div>
                      <h1 className="color17 display-3"><strong>Anthony (Onespiral)</strong></h1>
                      <sup className="color7">Chief Marketing Officer</sup>
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-4 col-12 profile">
                      <div className="text-center"><img className="img-responsive w-50" src={avatar} alt="" /></div>
                      <h1 className="color17 display-3"><strong>Stephen (Rasputin)</strong></h1>
                      <sup className="color14">Secretary/Board Member</sup>
                      
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-4 col-12 profile">
                      <div className="text-center"><img className="img-responsive w-50" src={avatar} alt="" /></div>
                      <h1 className="color17 display-3"><strong>Peter (Lestari)</strong></h1>
                      <sup className="color14">Marketing/Board Member</sup>
                      
                    </div>

                  </div>
              </div>
            </section>
          </div>
        </div>
      </div>
    </div>
    </section>
    <Footer />
    </div>
    );
    }
  }
  export default withRouter(AboutUs);
