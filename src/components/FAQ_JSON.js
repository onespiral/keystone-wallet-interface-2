const KEYSTONE_FAQ = [
    {
        q:"What is Keystone?",
        a:"Keystone is a currency that will be the primary form of payment in La Tortuga, Mexico. It will be used for all transactions once the development is fully operational. You will be able to rent hotel rooms, buy dinner, gamble, pay for excursions, and many more possibilities!"
    },
    {
        q:"What is the max coin supply?",
        a:"Keystone’s max supply is 150,000,000."
    },
    {
        q:"Can I mine Keystone?",
        a:"Yes, mining operations will be open to the public once the blockchain code is released. Mining software can be developed, or the mining program developed by the Keystone blockchain developers can be used."
    },
    {
        q:"Can I earn money with mining?",
        a:"There are many way’s to earn money using blockchain technology, Keystone just brings a few real world uses to the table."
    },
    {
        q:"Can I setup a node?",
        a:"Yes. (<a href=\"https://www.dropbox.com/s/y69alf9wenr3431/Setup Node.txt?dl=0\">Follow this wiki</a>)"
    },
    {
        q:"Where can I keep my Keystone?",
        a:"You don’t need to “keep your keystone” anywhere. They are always on the chain. 	Access can be had through any open node with private key signatures."
    },
    {
        q:"How do I access my Keystone?",
        a:"Access to Keystone is granted through using any web interface application. Specification for developing one can be found here. All nodes come with a wallet interface, it’s up to the node if the run it."
    },
    {
        q:"How do I secure my Keystone?",
        a:"Your Keystone are secured on the chain, by your private key. Keep you private key 	safe and your Keystone remains safe."
    },
    {
        q:"I lost my private key, is there a way to retrieve it?",
        a:"There is no way to return your private key."
    },
    {
        q:"How do I send Keystone?",
        a:"Send a transaction via any web interface by verifying you have the private key to do so."
    },
    {
        q:"What are the transaction fees?",
        a:"Transaction fees vary from node to node, and from pow to pos. See here for more information."
    },
    {
        q:"What is the lowest denomination of Keystone?",
        a:"Six decimal places, 0.000001."
    },
    {
        q:"Is there a mobile wallet?",
        a:"All wallets are mobile, as they are the private key, as long as you have it you can use 	any wallet interface to perform transactions."
    },
    {
        q:"What is Proof of Stake?",
        a:"Proof of stake (PoS) is a type of algorithm by which a cryptocurrency blockchain 	network aims to achieve distributed consensus. In PoS-based cryptocurrencies, the 	creator of the next block is chosen via various combinations of random selection and 	wealth or age (i.e., the stake)."
    },
    {
        q:"When does Proof of Stake start?",
        a:"Proof of Stake will start when circulation supply hits 35m, or you see a vampire."
    },
    {
        q:"What are the Proof of Stake rewards?",
        a:"Our POS system has not been worked out yet, therefor we cannot reveal the rewards yet."
    },
    {
        q:"Who owns Keystone?",
        a:"No one owns the chain code, the developer made that public, but Keystone Currency Inc. Owns the exchange."
    },
    {
        q:"Is the Keystone source public?",
        a:"We are keeping in private until we launch, then we will publish it and launch at the same time."
    },
    {
        q:"Where can I find the Roadmap?",
        a:"<a href=\"/Roadmap\">Check Roadmap</a>"
    },
    {
        q:"Where can I find the Whitepaper?",
        a:"<a href=\"/Litepaper\">Check Whitepaper</a>"
    },
    {
        q:"What is the Keystone Exchange?",
        a:"The Keystone Exchange is your source for keystone while others create their 	exchanges. We got the good's while you get the goods!"
    },
    {
        q:"Which trading pairs does the Keystone Exchange support?",
        a:"At the beginning the Keystone Exchange supports KEYS/FIAT. Later on there will be KEYS/BTC pairs and other cryptocurrencies."
    },
    {
        q:"What are the official sources of news from Keystone?",
        a:"<a href=\"https://twitter.com/Keystone_KEYS\">Twitter</a> | <a href=\"https://www.facebook.com/KeystoneCurrency/\">Facebook</a> | <a href=\"https://www.instagram.com/keystonecurrency/\">Instagram</a> | <a href=\"https://discord.gg/vjbXeHf\">Discord</a> | <a href=\"https://t.me/joinchat/AAAAAEyeJE3cAzcZjG8OHg\">Telegram</a> | <a href=\"https://www.reddit.com/user/KeystoneCurrency/\">Reddit</a>"
    },
    {
        q:"Will Keystone Currency be used only in La Tortuga? Or will Keystone Currency be developed and used in other regions later?",
        a:"Keystone will initially be developed for the La Tortuga development in Mexico. It is, however, a scalable project and there are future developments already earmarked. First and foremost though, we are focusing on La Tortuga."
    },
    {
        q:"How many people are working on the Keystone Currency project?",
        a:"We have a team of 13 actively working on this project ranging from marketers and writers to experienced web and programming devs. We cover all bases but we're always looking for top talent to contribute."
    },
    {
        q:"What sort of rig will i need to mine for the Bcrypt algo?",
        a:"We are using the bcrypt algorithm to enable fair mining for all. Bcrypt should make it good for all to mine, whether you have a powerful rig or not."
    },
    {
        q:"When is Keystone Currency getting listed on an exchange?",
        a:"After the coin is launched, we will start the process of getting listed on exchanges, whilst building our own exchange.  <br/> To get listed, we will need to prove our coins worth by way of a good product and a great community so this is something we are working on continuously."
    },
    {
        q:"How will moving from POW to POS be handled?",
        a:"At 35m coins in circulation, the code will switch over seamlessly to our PoS system.  The code for the switch has been written, but as for the actual PoS rewards and finer details, we are yet to confirm those.  We are building an economy around this coin, so we have to make sure we get it right.  We have until the coin is mined to 35m before we implement the PoS but we assure you this will be done long before we reach that."
    },
    {
        q:"Does the government support La Tortuga  and Keystone?",
        a:"If we did not have the support of local, state, and federal government. We could not make the statements that we have made. La Tortuga was an ongoing project well before the incorporation of Keystone. Keystone has now brought us a opportunity that meets our standards for our community. A true decentralized wealth system. To be blended into our 100&#37; off grid community."
    },
    {
        q:"Will there be a bounty program?",
        a:"Yes, later on there will be."
    },
    {
        q:"Will there be a downloadable wallet?",
        a:"Its a brand new blockchain written from scratch.  We are not forking or copying another coin with core wallet features.  We are starting with a web based wallet using RSA signatures.  This will allow us to be compatible on almost every device.  We will look to release downloadable wallets in the future. But with it being javascript based, you will be able to run a node or miner on most devices."
    }
];


const LATORTUGA_FAQ = [
    {
        q:"What is La Tortuga?",
        a:"La Tortuga will be the first eco-friendly, off-grid 21st century city. A full service community with hospitals attracting the world's best practitioners, schools staffed by the best educators, a place where utilities such as power and water, waste management, heating and air conditioning have been designed from the beginning to achieve the minimum carbon footprint whilst maximising the standard of living for all our residents."
    },
    {
        q:"Where is La Tortuga located?",
        a:"La Tortuga is located in Puerto Peñasco municipality in the state of Sonora, Mexico."
    },
    {
        q:"How much has been invested in the La Tortuga development thus far?",
        a:"Around $20,000,000.00 USD"
    },
    {
        q:"How much land will be developed?",
        a:"3,000 hectares total. Phase 1 will consist of 500 hectares"
    },
    {
        q:"What stage of development is the land in?",
        a:"All zoning has been approved on the entire property for development."
    },
    {
        q:"Will there be a Cruise port near La Tortuga?",
        a:"The cruise port is being built as we speak. The Federal Government has recently budgeted and approved $80,000,000.00 to continue construction. Plus a recent coalition between the Puerto Penasco Government and the the Arizona Government in the United States was formed with sole tasc of getting the pier finished."
    },
    {
        q:"Will the US Dollar or Mexican Peso be accepted at La Tortuga?",
        a:"Keystone Currency will be the only accepted form of payment accepted for commerce and investment in the development."
    },
    {
        q:"How will vendors keep up with fluctuating price?",
        a:"Prices will fluctuate and we understand that. Vendors will be encouraged to lock in a price every 12 hours. Our goal is to implement technology that will allow for streamlined shopping. Those who work at La Tortuga and are paid in KEYS will not have to wait for 2 weeks to receive a paycheck; this will help alleviate the burden of fluctuating price."
    }
];

module.exports = { KEYSTONE_FAQ, LATORTUGA_FAQ };