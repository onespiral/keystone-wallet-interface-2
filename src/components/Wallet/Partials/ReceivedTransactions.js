import React from 'react';
export default (props) => {
        return (<div className="card mx-0 my-0">
                    <div className="card-header color7-bg" id="ReceivedTransactionHeader">
                        <h3 className="mb-0 color4" data-toggle="collapse" data-target="#collapseReceivedTransaction" aria-expanded="true" aria-controls="collapseReceivedTransaction">
                            <strong className="display3">Received</strong> <span className="float-right"> Total: {props.transactions.length} </span>
                        </h3>
                    </div>
                    <div id="collapseReceivedTransaction" className="collapse hide" aria-labelledby="ReceivedTransactionHeader">
                        <div className="card-body mx-0 my-0 px-0 py-0">
                            {props.transactions.length ? props.transactions.map(transaction => {
                            return (<div className="card" key={transaction.transactionsIndex}>
                                <div className="card-header color8-bg">
                                    <div className="row color4">
                                        <div className="col"><small>INDEX {transaction.transactionsIndex}</small></div>
                                        <div className="col"><small>{transaction.amount} {"\u26B7"}</small></div>
                                        <div className="col"><small>{transaction.fee} {"\u26B7"} FEE</small><span className="float-right"><a href={`/Transactions/Index/${transaction.transactionsIndex}`}><i className="fa fa-search"></i></a></span></div>
                                    </div>
                                </div>    
                            </div>)}) : ''}
                        </div>
                    </div>
                </div>);
}