import React from 'react';
export default (props) => {
        return (
                <div className="card mx-0 my-0">
                    <div className="card-header color7-bg" id="SentTransactionHeader">
                        <h3 className="mb-0 color4" data-toggle="collapse" data-target="#collapseSentTransaction" aria-expanded="true" aria-controls="collapseSentTransaction">
                            <strong className="display3">Sent</strong> <span className="float-right"> Total: {props.transactions.length} </span>
                        </h3>
                    </div>
                    <div id="collapseSentTransaction" className="collapse hide" aria-labelledby="SentTransactionHeader">
                        <div className="card-body mx-0 my-0 px-0 py-0">
                        {props.transactions.length ? props.transactions.map(transaction => {
                            return (<div className="card" key={`sentTrx-id-${transaction.transactionsIndex}`}>
                                <div className="card-header color8-bg">
                                    <div className="row color4">
                                        <div className="col"><small>INDEX {transaction.transactionsIndex}</small></div>
                                        <div className="col"><small>{transaction.amount} {"\u26B7"}</small></div>
                                        <div className="col"><small>{transaction.fee} {"\u26B7"} FEE</small>
                                        <span className="float-right"><a href={`/Transactions/Index/${transaction.transactionsIndex}`} target="_default"><i className="fa fa-search"></i></a></span></div>
                                    </div>
                                </div>    
                            </div>)}) : ''}
                        </div>
                    </div>
                </div>
                );
};