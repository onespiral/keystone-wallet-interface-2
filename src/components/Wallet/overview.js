import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter} from 'react-router-dom';
import Header from './../Header';
import Footer from './../Footer';
import axios from 'axios';
import HOST_URL from '../../configs';
import SentTransactions from './Partials/SentTransactions';
import ReceivedTransactions from './Partials/ReceivedTransactions';
import AwardedBlocks from './Partials/AwardedBlocks';
class WalletOverview extends Component {
    state = {
      balance:0,
      blocks:[],
      totalBlocks:0,
      includesGenesis:false,
      transactions:[],
      sentTransactions:[],
      receivedTransactions:[],
    };
    componentWillMount(){
      const updateOverview = () =>{
        axios.get(`${HOST_URL}/Wallet/Balance`, {headers:{Authorization:this.props.auth.wallet.walletPublicKey.split('\n').join('\\n')}})
        .then((response) => {
          this.setState({
           balance:response.data.balance
          });  
        })
        .catch(err => {console.log(err);});
        /* TODO :: NEEDS LIMITS*/
        axios.get(`${HOST_URL}/Wallet/Transaction`, {headers:{Authorization:this.props.auth.wallet.walletPublicKey.split('\n').join('\\n')}})
        .then((response) => {
          console.log(response);          
          this.setState({
           transactions:response.data.transactions,
           sentTransactions: response.data.transactions.filter(transaction => transaction.sender === this.props.auth.wallet.walletPublicKey),
           receivedTransactions: response.data.transactions.filter(transaction => transaction.receiver === this.props.auth.wallet.walletPublicKey)
          });
        })
        .catch(err => {console.log(err);});
              
        /* TODO :: NEEDS LIMITS*/
        axios.get(`${HOST_URL}/Wallet/Awards`, {headers:{Authorization:this.props.auth.wallet.walletPublicKey.split('\n').join('\\n')}})
        .then((response) => {
          console.log(response);
          this.setState({
           blocks:response.data.blocks,
           totalBlocks:response.data.totalBlocks,
           includesGenesis:response.data.includesGenesis
          });  
        })
        .catch(err => {console.log(err);});
      };
      updateOverview();
      setInterval(updateOverview.bind(this), 35000);
    }
    
    render() {
      const imageQR = this.props.auth.wallet.getQRImage();
      return (
          <div>
            <section>
                <Header />
            </section>
            <section>
                <div className="container-fluid" id="aboutus">
                    <div className="container">
                        <div className="row ">
                            <div className="col text-center">
                                <h1 className="green">Wallet Explorer</h1>
                            </div>
                        </div>
                        <div className="card">
                            <div className="card-header color15-bg color5 text-center">
                                <div className="row">
                                    <div className="col-12 col-sm-12 col-md-3 col-lg-3 text-right pl-0">
                                        <h3>Balance</h3>
                                        <h4>{this.state.balance ? this.state.balance: 0} {"\u26B7"}</h4>
                                    </div>
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6 my-0 py-0 px-1 align-self-center">
                                        <h3>Public Key</h3>
                                        <pre className="color4 pt-2">{this.props ? this.props.auth.wallet.walletPublicKey : ''}</pre>
                                    </div>
                                    <div className="col-12 col-sm-12 col-md-3 col-lg-3 align-self-center pr-0">
                                        <h3 className="text-left">QR Code</h3>
                                        <div className="thumbnail" dangerouslySetInnerHTML={{ __html: imageQR }} />
                                    </div>
                              </div>
                            </div>
                            <div className="card-body mx-0 my-0 px-0 py-0">
                                <div className="row">
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6 pr-0">
                                      <SentTransactions transactions={this.state.sentTransactions} />
                                    </div>
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6 pl-0">
                                      <ReceivedTransactions transactions={this.state.receivedTransactions} />
                                    </div>
                                    <div className="col">
                                      <AwardedBlocks blocks={this.state.blocks || []}  totalBlocks={this.state.totalBlocks || 'N/A'}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section>  
                <Footer />
            </section>
          </div>);
    }
  }
  
  const mapStateToProps = state => {
  // add spent tansactions and unspent transactions
  return {
    auth: state.auth,
    error: state.auth.error
  };
};

export default withRouter(connect(mapStateToProps)(WalletOverview));
