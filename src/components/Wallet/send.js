import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { reduxForm, Field } from 'redux-form';
import { sendTransaction } from '../../actions';
import Header from '../Header';
import Footer from '../Footer';
class SendTransaction extends Component {
  renderAlert = () => {
    if (!this.props.error) return null;
    if(this.props.error.response !== undefined){
      return <h3><strong>{this.props.error.response.data.status}</strong>{this.props.error.response.data.message}</h3>;
    }
    return <h3>{console.log(this.props.error)}</h3>;
  };
  handleFormSubmit = ({ receiver, amount, stakeAmount, message }) => {
    const { auth, history } = this.props;
    this.setState({progress:'inProgress', error:null });
    this.props.sendTransaction(receiver, auth.wallet, Number(amount).toFixed(6), stakeAmount, message, history);
    this.setState({progress:null});
  };
  render() {
    const { handleSubmit } = this.props;
    return (
      <div>
      <Header/>
    	<div className="container-fluid" id="aboutus">
        <div className="container">

      <section className="grey_bg">
 				<div className="row">
 					<div className="col">
 						<h3 className="green">SEND KEYSTONE</h3>	
 					</div>	
 				</div>	
 				<form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
 				<div className="row">
 					<div className="col">
 							<div className="form-check form-group">
							  <label className="form-check-label color16" htmlFor="exampleRadios1">
								Make sure this is correct or you will lose your Keystone.
							  </label>
              </div>
						  <div className="form-group">
                <Field name="receiver" component="input" type="text"  className="form-control" placeholder="Add reciver public key"/>
						  </div>
						  <div className="form-row justify-content-center">
                <div className="col">
                  <Field name="amount" component="input" type="text" className="form-control" placeholder="US$"/>
                </div>
                <div className="badge badge-secondary align-self-center color8-bg">=</div>
                <div className="col">
                  <Field name="amount" component="input" type="text" className="form-control" placeholder="Keystone"/>
                </div>
						  </div>
						  <div className="m-3"></div>
						  <div className="form-group">
                <Field name="message" component="textarea" type="text" className="form-control" rows="3" placeholder="Message" />
						  </div>
 					</div>
 				</div>
 				<div className="row">
					<div className="col-12 col-sm-6 col-md-3 col-lg-3">
            <button className="btn btn-lg green_bg btn-block" action="submit">Send</button>
            {this.renderAlert()}
					</div>
 				</div>
			</form>	
 			</section>
      </div>
      </div>
      <Footer />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    users: state.users,
    auth: state.auth,
    error: state.transaction.error,
    transaction: state.transactions
  };
};

SendTransaction = withRouter(connect(mapStateToProps, { sendTransaction })(SendTransaction));

export default reduxForm({
  form: 'send-transaction',
  fields: ['receiver', 'amount', 'stakeAmount', 'message']
})(SendTransaction);
