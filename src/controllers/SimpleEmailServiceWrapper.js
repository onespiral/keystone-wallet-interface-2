const nodemailer = require('nodemailer');
module.exports = class SimpleEmailServiceWrapper {
    constructor(props){
      this.service = props.service || 'gmail',
      this.auth = props.auth || {
          user: null,
          pass: null
        }

      this.transporter = nodemailer.createTransport({
        service: this.service,
        auth: this.auth
      });
  
      this.mailOptions = props.mailOptions || {
        from: this.auth.user,
        replyTo: this.auth.user,
        to: null,
        subject: null,
        text: null,
        html: null
      };
    }
    
    sendMail(cb){
      return this.transporter.sendMail(this.mailOptions, cb);
    }
  }